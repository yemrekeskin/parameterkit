using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ParameterKit.Tests
{
    [TestClass]
    public class ParameterTests
    {
        [TestMethod]
        public void GettingContinents()
        {
            var paramService = new ParameterService();
            var result = paramService.GetContinents();

            Assert.AreNotEqual(null, result);
        }

        [TestMethod]
        public void GettingCountries()
        {
            var paramService = new ParameterService();
            var result = paramService.GetCountries();

            Assert.AreNotEqual(null, result);
        }

        [TestMethod]
        public void GettingCurrencies()
        {
            var paramService = new ParameterService();
            var result = paramService.GetCurrencies();

            Assert.AreNotEqual(null, result);
        }

        [TestMethod]
        public void GettingLanguages()
        {
            var paramService = new ParameterService();
            var result = paramService.GetLanguages();

            Assert.AreNotEqual(null, result);
        }

        [TestMethod]
        public void GettingTurkeyCities()
        {
            var paramService = new ParameterService();
            var result = paramService.GetTurkeyCities();

            Assert.AreNotEqual(null, result);
        }

        [TestMethod]
        public void GettingUniversities()
        {
            var paramService = new ParameterService();
            var result = paramService.GetUniversities();

            Assert.AreNotEqual(null, result);
        }
    }
}