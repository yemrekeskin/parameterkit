﻿using ClientApp.Models;
using Microsoft.AspNetCore.Mvc;
using ParameterKit;
using System.Diagnostics;

namespace ClientApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IParameterService parameterService;
        public HomeController(IParameterService parameterService)
        {
            this.parameterService = parameterService;
        }

        public IActionResult Index()
        {
            var continents = parameterService.GetContinents();
            var country = parameterService.GetCountries();
            var turkeyCities = parameterService.GetTurkeyCities();
            var languages = parameterService.GetLanguages();
            var currencies = parameterService.GetCurrencies();
            var university = parameterService.GetUniversities();

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}