﻿using System.Collections.Generic;

namespace ParameterKit
{
    public interface IParameterService
    {
        List<Country> GetCountries();

        List<Currency> GetCurrencies();

        List<Continent> GetContinents();

        List<Language> GetLanguages();

        List<TurkeyCity> GetTurkeyCities();

        List<University> GetUniversities();
    }
}