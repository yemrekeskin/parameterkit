﻿namespace ParameterKit
{
    public class TurkeyCity
        : Model
    {
        public int Plate { get; set; }
        public string Name { get; set; }
        public string Population { get; set; }
        public string Region { get; set; }

        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}