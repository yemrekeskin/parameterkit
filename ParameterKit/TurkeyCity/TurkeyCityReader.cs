﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ParameterKit
{
    internal class TurkeyCityReader
        : DataReader<TurkeyCity>
    {
        internal override List<TurkeyCity> Read()
        {
            List<TurkeyCity> turkeyCities = new List<TurkeyCity>();

            var assembly = typeof(TurkeyCity).GetTypeInfo().Assembly;
            var path = assembly.GetManifestResourceStream("ParameterKit.TurkeyCity.TurkeyCity.json");

            using (StreamReader reader = new StreamReader(path))
            {
                var content = reader.ReadToEnd();
                turkeyCities = JsonConvert.DeserializeObject<List<TurkeyCity>>(content);
            }

            return turkeyCities;
        }
    }
}