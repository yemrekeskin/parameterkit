﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ParameterKit
{
    internal class CountryReader
        : DataReader<Country>
    {
        internal override List<Country> Read()
        {
            List<Country> countries = new List<Country>();

            var assembly = typeof(Country).GetTypeInfo().Assembly;
            var path = assembly.GetManifestResourceStream("ParameterKit.Country.country.json");

            using (StreamReader reader = new StreamReader(path))
            {
                var content = reader.ReadToEnd();
                countries = JsonConvert.DeserializeObject<List<Country>>(content);
            }

            return countries;
        }
    }
}