﻿namespace ParameterKit
{
    public class Country
        : Model
    {
        public string Name { get; set; }
        public string AreaCode { get; set; }
        public string ISOCode { get; set; }
    }
}