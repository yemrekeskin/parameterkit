﻿namespace ParameterKit
{
    public class Continent
        : Model
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}