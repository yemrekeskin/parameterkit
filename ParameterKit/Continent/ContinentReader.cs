﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ParameterKit
{
    internal class ContinentReader
        : DataReader<Continent>
    {
        internal override List<Continent> Read()
        {
            List<Continent> continents = new List<Continent>();

            var assembly = typeof(Continent).GetTypeInfo().Assembly;
            var path = assembly.GetManifestResourceStream("ParameterKit.Continent.continent.json");

            using (StreamReader reader = new StreamReader(path))
            {
                var content = reader.ReadToEnd();
                continents = JsonConvert.DeserializeObject<List<Continent>>(content);
            }

            return continents;
        }
    }
}