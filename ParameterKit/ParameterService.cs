﻿using System.Collections.Generic;

namespace ParameterKit
{
    public class ParameterService
        : IParameterService
    {
        private readonly DataReader<Continent> ContinentReader;
        private readonly DataReader<Country> CountryReader;
        private readonly DataReader<Currency> CurrencyReader;
        private readonly DataReader<Language> LanguageReader;
        private readonly DataReader<TurkeyCity> TurkeyCityReader;
        private readonly DataReader<University> UniversityReader;

        public ParameterService()
        {
            this.ContinentReader = new ContinentReader();
            this.CountryReader = new CountryReader();
            this.CurrencyReader = new CurrencyReader();
            this.LanguageReader = new LanguageReader();
            this.TurkeyCityReader = new TurkeyCityReader();
            this.UniversityReader = new UniversityReader();
        }

        public List<Continent> GetContinents()
        {
            return this.ContinentReader.Read();
        }

        public List<Country> GetCountries()
        {
            return this.CountryReader.Read();
        }

        public List<Currency> GetCurrencies()
        {
            return this.CurrencyReader.Read();
        }

        public List<Language> GetLanguages()
        {
            return this.LanguageReader.Read();
        }

        public List<TurkeyCity> GetTurkeyCities()
        {
            return this.TurkeyCityReader.Read();
        }

        public List<University> GetUniversities()
        {
            return this.UniversityReader.Read();
        }
    }
}