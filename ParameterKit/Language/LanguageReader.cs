﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ParameterKit
{
    internal class LanguageReader
        : DataReader<Language>
    {
        internal override List<Language> Read()
        {
            List<Language> languages = new List<Language>();

            var assembly = typeof(Language).GetTypeInfo().Assembly;
            var path = assembly.GetManifestResourceStream("ParameterKit.Language.language.json");

            using (StreamReader reader = new StreamReader(path))
            {
                var content = reader.ReadToEnd();
                languages = JsonConvert.DeserializeObject<List<Language>>(content);
            }

            return languages;
        }
    }
}