﻿namespace ParameterKit
{
    public class Language
        : Model
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string NativeName { get; set; }
    }
}