﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ParameterKit
{
    internal class UniversityReader
        : DataReader<University>
    {
        internal override List<University> Read()
        {
            List<University> languages = new List<University>();

            var assembly = typeof(University).GetTypeInfo().Assembly;
            var path = assembly.GetManifestResourceStream("ParameterKit.University.university.json");

            using (StreamReader reader = new StreamReader(path))
            {
                var content = reader.ReadToEnd();
                languages = JsonConvert.DeserializeObject<List<University>>(content);
            }

            return languages;
        }
    }
}