﻿using System;
using System.Collections.Generic;

namespace ParameterKit
{
    public class University
        : Model
    {
        public string Country { get; set; }
        public string Name { get; set; }

        public List<String> Web_Pages { get; set; }
    }
}