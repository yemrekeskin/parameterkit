﻿using System.Collections.Generic;

namespace ParameterKit
{
    internal abstract class DataReader<T>
        where T : Model
    {
        internal abstract List<T> Read();
    }
}