﻿namespace ParameterKit
{
    public class Currency
        : Model
    {
        public string Code { get; set; }
        public int Exponent { get; set; }
        public string Name { get; set; }
        public int NumericCode { get; set; }
        public string CountryISOCode { get; set; }
    }
}