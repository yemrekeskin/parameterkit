﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ParameterKit
{
    internal class CurrencyReader
        : DataReader<Currency>
    {
        internal override List<Currency> Read()
        {
            List<Currency> currencies = new List<Currency>();

            var assembly = typeof(Currency).GetTypeInfo().Assembly;
            var path = assembly.GetManifestResourceStream("ParameterKit.Currency.currency.json");

            using (StreamReader reader = new StreamReader(path))
            {
                var content = reader.ReadToEnd();
                currencies = JsonConvert.DeserializeObject<List<Currency>>(content);
            }

            return currencies;
        }
    }
}