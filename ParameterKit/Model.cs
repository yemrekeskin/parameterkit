﻿using System;

namespace ParameterKit
{
    public abstract class Model
    {
        public Guid Id { get; set; }
        public bool Active { get; set; }

        public Model()
        {
            this.Id = Guid.NewGuid();
        }
    }
}